<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteNikitaContentPlaces extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nikita_content_places');
    }
    
    public function down()
    {
        Schema::create('nikita_content_places', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 255)->nullable();
            $table->string('image', 255)->nullable();
        });
    }
}
