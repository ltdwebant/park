<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentFriends extends Migration
{
    public function up()
    {
        Schema::table('nikita_content_friends', function($table)
        {
            $table->string('link')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('nikita_content_friends', function($table)
        {
            $table->dropColumn('link');
        });
    }
}
