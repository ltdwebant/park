<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentProducts extends Migration
{
    public function up()
    {
        Schema::table('nikita_content_products', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('nikita_content_products', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
