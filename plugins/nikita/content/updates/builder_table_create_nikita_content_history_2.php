<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNikitaContentHistory2 extends Migration
{
    public function up()
    {
        Schema::create('nikita_content_history', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('year');
            $table->string('content')->nullable();
            $table->string('image')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nikita_content_history');
    }
}
