<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentOther extends Migration
{
    public function up()
    {
        Schema::table('nikita_content_other', function($table)
        {
            $table->text('content')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('nikita_content_other', function($table)
        {
            $table->string('content', 255)->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
