<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNikitaContentSlider extends Migration
{
    public function up()
    {
        Schema::create('nikita_content_slider', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('image');
            $table->string('title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nikita_content_slider');
    }
}
