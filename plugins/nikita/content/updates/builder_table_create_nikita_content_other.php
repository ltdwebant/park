<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNikitaContentOther extends Migration
{
    public function up()
    {
        Schema::create('nikita_content_other', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('content')->nullable();
            $table->string('image')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nikita_content_other');
    }
}
