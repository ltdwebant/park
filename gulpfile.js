var gulp = require('gulp'), // Сообственно Gulp JS
    less = require('gulp-less');


gulp.task('default', function() {

    gulp.watch('less/*.less',['less']);
});

gulp.task('less',function(){
    gulp.src('less/*.less')
        .pipe(less())
        .pipe(gulp.dest('themes/new-design/assets/css'))

});
