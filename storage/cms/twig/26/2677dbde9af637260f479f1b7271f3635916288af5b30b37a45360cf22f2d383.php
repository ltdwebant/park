<?php

/* /Users/nikita/WebstormProjects/park/themes/new-design/pages/attractions.htm */
class __TwigTemplate_4c3a5f039696d1007a9324cd06c56c777f0197f09724a76a377f7f8b5c8a77d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["attractions"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "records", array());
        // line 2
        $context["displayColumn"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "displayColumn", array());
        // line 3
        $context["noRecordsMessage"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "noRecordsMessage", array());
        // line 4
        $context["detailsPage"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsPage", array());
        // line 5
        $context["detailsKeyColumn"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsKeyColumn", array());
        // line 6
        $context["detailsUrlParameter"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsUrlParameter", array());
        // line 7
        echo "
<div class=\"page-wrapper\">

    <!--<div class=\"page__title standard-block\">АТТРАКЦИОНЫ</div>-->


    <div class=\"attractions-page__title\">

        <div class=\"attractions-page__title__image\">
            <img src=\"/themes/new-design/assets/images/attractions_grey.png\">

        </div>

        <div class=\"attractions-page__title__text\">

            <a href=\"/attractions/price-list\"><div class=\"attractions-page__title__text__link\">
                ПРЕЙСКУРАНТ ЦЕН НА БИЛЕТЫ В <span class=\"color-green\">ПАРКЕ</SPAN>
            </div>
            </a>
            В нашем парке огромный выбор аттракционов на любой вкус и возраст. Большое разнообразие детских зон и аттракционов.
            Мы можем предложить как семейные аттракционы, так и для любителей экстрима.

        </div>


    </div>


    <div class=\"standard-block\" style=\"position: relative;\">

        <div class=\"attractions__arrow-block\">

            <div class=\"attractions__arrow\">
                <img src=\"themes/main-theme/assets/images/arrow-right.svg\" class=\"attractions__arrow\">
            </div >
        </div>

        ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attractions"]) ? $context["attractions"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 45
            echo "
        <div class=\"attractions\" id=\"attractions-block-";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\">

            <div class=\"attractions__block attractions__left-block\" style=\"background-image: url('storage/app/media";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "image", array()), "html", null, true);
            echo "')\">
            </div>

            <div class=\"attractions__block attractions__right-block\">
                <div class=\"attractions__right-block__content\">

                    <div class=\"attraction__title\">";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
            echo "</div>


                    ";
            // line 57
            echo $this->getAttribute($context["item"], "content", array());
            echo "

                </div>
            </div>

        </div>


        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "
        <div style=\"clear: both\"></div>

    </div>

</div>

<script>
    var i=0;

    var idArray = [];

    \$(function(){

        ";
        // line 80
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attractions"]) ? $context["attractions"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 81
            echo "        idArray[idArray.length] = '";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "';
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 83
        echo "
        \$('#attractions-block-'+idArray[i]).addClass('attractions_active');

    });

    \$('.attractions__arrow-block').click(function(){
        i++;

        if (i == idArray.length){
            i = 0;
        }

        \$('.attractions_active').removeClass('attractions_active');

        \$('#attractions-block-'+idArray[i]).addClass('attractions_active');


    })

</script>";
    }

    public function getTemplateName()
    {
        return "/Users/nikita/WebstormProjects/park/themes/new-design/pages/attractions.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 83,  132 => 81,  128 => 80,  112 => 66,  97 => 57,  91 => 54,  82 => 48,  77 => 46,  74 => 45,  70 => 44,  31 => 7,  29 => 6,  27 => 5,  25 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% set attractions = builderList.records %}*/
/* {% set displayColumn = builderList.displayColumn %}*/
/* {% set noRecordsMessage = builderList.noRecordsMessage %}*/
/* {% set detailsPage = builderList.detailsPage %}*/
/* {% set detailsKeyColumn = builderList.detailsKeyColumn %}*/
/* {% set detailsUrlParameter = builderList.detailsUrlParameter %}*/
/* */
/* <div class="page-wrapper">*/
/* */
/*     <!--<div class="page__title standard-block">АТТРАКЦИОНЫ</div>-->*/
/* */
/* */
/*     <div class="attractions-page__title">*/
/* */
/*         <div class="attractions-page__title__image">*/
/*             <img src="/themes/new-design/assets/images/attractions_grey.png">*/
/* */
/*         </div>*/
/* */
/*         <div class="attractions-page__title__text">*/
/* */
/*             <a href="/attractions/price-list"><div class="attractions-page__title__text__link">*/
/*                 ПРЕЙСКУРАНТ ЦЕН НА БИЛЕТЫ В <span class="color-green">ПАРКЕ</SPAN>*/
/*             </div>*/
/*             </a>*/
/*             В нашем парке огромный выбор аттракционов на любой вкус и возраст. Большое разнообразие детских зон и аттракционов.*/
/*             Мы можем предложить как семейные аттракционы, так и для любителей экстрима.*/
/* */
/*         </div>*/
/* */
/* */
/*     </div>*/
/* */
/* */
/*     <div class="standard-block" style="position: relative;">*/
/* */
/*         <div class="attractions__arrow-block">*/
/* */
/*             <div class="attractions__arrow">*/
/*                 <img src="themes/main-theme/assets/images/arrow-right.svg" class="attractions__arrow">*/
/*             </div >*/
/*         </div>*/
/* */
/*         {% for item in attractions %}*/
/* */
/*         <div class="attractions" id="attractions-block-{{item.id}}">*/
/* */
/*             <div class="attractions__block attractions__left-block" style="background-image: url('storage/app/media{{item.image}}')">*/
/*             </div>*/
/* */
/*             <div class="attractions__block attractions__right-block">*/
/*                 <div class="attractions__right-block__content">*/
/* */
/*                     <div class="attraction__title">{{item.title}}</div>*/
/* */
/* */
/*                     {{item.content|raw}}*/
/* */
/*                 </div>*/
/*             </div>*/
/* */
/*         </div>*/
/* */
/* */
/*         {% endfor %}*/
/* */
/*         <div style="clear: both"></div>*/
/* */
/*     </div>*/
/* */
/* </div>*/
/* */
/* <script>*/
/*     var i=0;*/
/* */
/*     var idArray = [];*/
/* */
/*     $(function(){*/
/* */
/*         {% for item in attractions %}*/
/*         idArray[idArray.length] = '{{item.id}}';*/
/*         {% endfor %}*/
/* */
/*         $('#attractions-block-'+idArray[i]).addClass('attractions_active');*/
/* */
/*     });*/
/* */
/*     $('.attractions__arrow-block').click(function(){*/
/*         i++;*/
/* */
/*         if (i == idArray.length){*/
/*             i = 0;*/
/*         }*/
/* */
/*         $('.attractions_active').removeClass('attractions_active');*/
/* */
/*         $('#attractions-block-'+idArray[i]).addClass('attractions_active');*/
/* */
/* */
/*     })*/
/* */
/* </script>*/
